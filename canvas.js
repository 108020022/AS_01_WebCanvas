var status = 'Brush'; //initial status is Brush

var originX = 0;
var originY = 0;
var destinationX = 0;
var destinationY = 0;

var MouseKeepPress = false;

var rec_width;
var rec_height;
var abs_rec_width;
var abs_rec_height;

var origin_canvas;
var temp_canvas;
var CanvasHistory_undo = [];
var CanvasHistory_redo = [];

var last_step_is_undo = false;
var input_the_text = true;

var text_content = "";

var main_color;
var main_font_family;
var main_font_size;
var main_stroke_range;

var cur_img = 'Brush.png';


function change_status(new_status){
    status = new_status;
    cur_img = status + '.png';
    console.log(status);
}

window.onload = function() {
    main();
} 


function main(){
    
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    var TextBox = document.getElementById("TextBox");

    var color_canvas = document.getElementById("color_choose");
    var color_ctx = color_canvas.getContext("2d");

    var font_family = document.getElementById("font_family");
    var font_size = document.getElementById("font_size");

    var stroke_range = document.getElementById("stroke_range");

    ctx.lineWidth = 10;

    ColorRect(color_ctx);
    ColorBox(color_ctx, 'red');

    origin_canvas = ctx.getImageData(0, 0, canvas.width, canvas.height);
    CanvasHistory_undo.push( origin_canvas );

    canvas.addEventListener('mouseenter', function(event){
        canvas.style['cursor'] = "url('" + cur_img + "') 8 20, auto";      
    });
    
    document.getElementById('undo').onclick = function Undo(){
        /*console.log(CanvasHistory_undo.length);*/
        if(CanvasHistory_undo.length > 1){
            CanvasHistory_redo.push( CanvasHistory_undo.pop() );
            temp_canvas = CanvasHistory_undo[ CanvasHistory_undo.length - 1];
            ctx.putImageData(temp_canvas, 0, 0);

        }
        else if(CanvasHistory_undo.length == 1)
            ctx.putImageData(origin_canvas, 0, 0);
        
        last_step_is_undo = true;
    };
    document.getElementById('redo').onclick = function Redo(){
        if(CanvasHistory_redo.length !== 0){
            temp_canvas = CanvasHistory_redo.pop();
            ctx.putImageData(temp_canvas, 0, 0);
            CanvasHistory_undo.push(temp_canvas);
        }
    };

    document.getElementById('reset').onclick = function Reset(){
        alert('Are you sure to reset?');
        ctx.putImageData(origin_canvas, 0, 0);
        CanvasHistory_undo = [];
        CanvasHistory_redo = [];

    };
    
    document.getElementById('download').onclick = function Download(){
        document.getElementById('download').href = canvas.toDataURL();
        document.getElementById('download').download = "MyCanvas.png";
    };

    document.getElementById('upload').onchange = function Upload(){
        var img = new Image();
        img.onload = function draw() {
            canvas.width = this.width;
            canvas.height = this.height;
            ctx.drawImage(this, 0,0);
        }
        img.src = URL.createObjectURL(this.files[0]);
    };

    document.getElementById("font_family").onchange = function Change_font_family(ctx){
        main_font_family = font_family.options[font_family.selectedIndex].value;  
    };

    document.getElementById("font_size").onchange = function Change_font_size(ctx){
        main_font_size = font_size.options[font_size.selectedIndex].value;  
    };

    document.getElementById("stroke_range").onchange = function Change_stroke_range(ctx){
        main_stroke_range = stroke_range.value;  
    };
    
    canvas.addEventListener('mousedown', function(event){
        if(status == 'Brush')
            Brush_mousedown(event, ctx);
        else if(status == 'Eraser')
            Eraser_mousedown(event, ctx);
        else if(status == 'Rectangle')
            Rectangle_mousedown(event, ctx, canvas);
        else if(status == 'Circle')
            Circle_mousedown(event, ctx, canvas);
        else if(status == 'Triangle')
            Triangle_mousedown(event, ctx, canvas);
        else if(status == 'Text')
            Text_mousedown(event, ctx, canvas,TextBox);      
    });

    color_canvas.addEventListener('mousedown', function(event){
        originX = event.offsetX;
        originY = event.offsetY;

        var rgb_list = '#000';
        if(originX >= 0 && originX < 40 && originY >= 0 && originY < 170){
            rgb_list = RGB_of_that_point(color_ctx, 'rect');
            ColorBox(color_ctx, 'rgba(' + rgb_list + ')');
        } else if( originX >= 50 && originX < 300 && originY >= 0 && originY < 170 ) {
            rgb_list = RGB_of_that_point(color_ctx, 'box');
        }


        main_color = 'rgb(' + rgb_list + ')';  
        ctx.strokeStyle =  main_color;
        console.log(main_color);
        
    });

    color_ctx.fillStyle = 'white';
    color_ctx.fillRect(40, 0, 10, 170);

    canvas.addEventListener('mousemove', function(event){
        if(status == 'Brush')
            Brush_mousemove(event, ctx);
        else if(status == 'Eraser')
            Eraser_mousemove(event, ctx, canvas); 
        else if(status == 'Rectangle')
            Rectangle_mousemove(event, ctx, canvas);
        else if(status == 'Circle')
            Circle_mousemove(event, ctx, canvas);
        else if(status == 'Triangle')
            Triangle_mousemove(event, ctx);    

        
            
    });

    window.addEventListener('mouseup', function(event){
        if(status == 'Brush')
            Brush_mouseup(ctx, canvas);
        else if(status == 'Eraser')
            Eraser_mouseup(ctx, canvas); 
        else if(status == 'Rectangle')
            Rectangle_mouseup(ctx, canvas);
        else if(status == 'Circle')
            Circle_mouseup(ctx, canvas);
        else if(status == 'Triangle')
            Triangle_mouseup(ctx, canvas);     
    

        
            
    });

    canvas.addEventListener('mouseup', function(event){
        
        
        temp_canvas = ctx.getImageData(0, 0, canvas.width, canvas.height);
        CanvasHistory_undo.push(temp_canvas);
        console.log(CanvasHistory_undo.length);
        
        if(last_step_is_undo){   
            CanvasHistory_redo = [];
            last_step_is_undo = false;
        }
        
    });

}

function ColorRect(color_ctx) {
    var gradientBar = color_ctx.createLinearGradient(0, 0, 0, 170);
    gradientBar.addColorStop(0, '#f00');
    gradientBar.addColorStop(1 / 6, '#f0f');
    gradientBar.addColorStop(2 / 6, '#00f');
    gradientBar.addColorStop(3 / 6, '#0ff');
    gradientBar.addColorStop(4 / 6, '#0f0');
    gradientBar.addColorStop(5 / 6, '#ff0');
    gradientBar.addColorStop(1, '#f00');

    color_ctx.fillStyle = gradientBar;
    color_ctx.fillRect(0, 0, 40, 170);
}
function ColorBox(color_ctx, color) {
    var gradientBar = color_ctx.createLinearGradient(50, 0, 300, 0);
    gradientBar.addColorStop(1, color);
    gradientBar.addColorStop(0, 'rgba(255,255,255,1)');
    color_ctx.fillStyle = gradientBar;
    color_ctx.fillRect(50, 0 , 250, 170);

    var gradientBox = color_ctx.createLinearGradient(0, 0, 0, 170);
    gradientBox.addColorStop(0, 'rgba(0,0,0,0)');
    gradientBox.addColorStop(1, 'rgba(0,0,0,1)');
    color_ctx.fillStyle = gradientBox;
    color_ctx.fillRect(50, 0 , 250, 170);

}

function RGB_of_that_point(color_ctx, str) {
    var imageData;
    if(str == 'rect'){
        imageData = color_ctx.getImageData(0, 0, 40, 170);
    } else {
        imageData = color_ctx.getImageData(0, 0, 300, 170);
    }

    var data = imageData.data;
    console.log(originX);
    console.log(originY);
    var index = (originY * imageData.width + originX) * 4;
    return [
        data[index],
        data[index + 1],
        data[index + 2],
        (data[index + 3] / 255).toFixed(2)
    ];

}

//Text function
function Text_mousedown (event, ctx) {
    if(input_the_text){  //輸入東西
        originX = event.offsetX;
        originY = event.offsetY;

        TextBox.style['left'] = originX.toString();
        TextBox.style['top'] = originY.toString();

        input_the_text = false;
        TextBox.style['z-index'] = 6;
        TextBox.style['border'] = "1px dashed #000000";

    } else {   //印東西上去
        if(TextBox.value == "")
            CanvasHistory_undo.pop();
        text_content = TextBox.value;
        TextBox.style['z-index'] = 1;
        TextBox.value = "";
        input_the_text = true;
        Draw_The_Text(ctx);
        TextBox.style['border'] = "transparent";
    }
    
}

function Draw_The_Text (ctx){
    ctx.fillStyle = "red";

    ctx.save();
    ctx.beginPath();
    ctx.font = main_font_size + 'px ' + main_font_family;
    ctx.fillStyle = main_color;
    console.log(main_font_family);
    ctx.fillText(text_content, originX, originY);

    ctx.restore();
    ctx.closePath();
}


//Brush function
function Brush_mousedown (event, ctx) {
    MouseKeepPress = true;
    originX = event.offsetX;
    originY = event.offsetY;

    ctx.strokeStyle = main_color;
    ctx.lineWidth = main_stroke_range;
}

function Brush_mousemove (event, ctx) {
    if(MouseKeepPress){
        destinationX = event.offsetX;
        destinationY = event.offsetY; 

        

        ctx.beginPath();
        ctx.moveTo(originX, originY);
        ctx.lineTo(destinationX, destinationY);
        ctx.stroke();

        originX = destinationX;
        originY = destinationY;
    }
    else
        return;
}

function Brush_mouseup (ctx, canvas) {
    MouseKeepPress = false;

}

//Eraser function
//會閃爍
function Eraser_mousedown (event, ctx) {
    MouseKeepPress = true;
    originX = event.offsetX;
    originY = event.offsetY;
    ctx.clearRect(originX - 10, originY - 10,20,20);
    ctx.lineWidth = main_stroke_range;
}

function Eraser_mousemove (event, ctx, canvas) {
    if(MouseKeepPress){
        destinationX = event.offsetX;
        destinationY = event.offsetY; 
        ctx.clearRect(destinationX - 10,destinationY - 10,20,20);

        ctx.save();
        ctx.beginPath();
        var slope = (destinationY - originY) / (destinationX - originX);
        if(slope<0){
            ctx.moveTo(originX + 10, originY - 10);
            ctx.lineTo(destinationX + 10, destinationY - 10);
            ctx.lineTo(destinationX - 10, destinationY + 10);
            ctx.lineTo(originX - 10, originY + 10);
            ctx.closePath();
            ctx.clip();
            ctx.clearRect(0,0,canvas.width,canvas.height);
            ctx.restore();
        }
        else{
            ctx.moveTo(originX - 10, originY - 10);
            ctx.lineTo(destinationX - 10, destinationY - 10);
            ctx.lineTo(destinationX + 10, destinationY + 10);
            ctx.lineTo(originX + 10, originY + 10);
            ctx.closePath();
            ctx.clip();
            ctx.clearRect(0,0,canvas.width,canvas.height);
            ctx.restore();
        }

        originX = destinationX;
        originY = destinationY;
        
    }
    else
        return;
}

function Eraser_mouseup (ctx, canvas) {
    MouseKeepPress = false;


}



//Rectangle function
function Rectangle_mousedown (event, ctx, canvas) {
    MouseKeepPress = true;
    originX = event.offsetX;
    originY = event.offsetY;

    temp_canvas = ctx.getImageData(0, 0, canvas.width, canvas.height);
    ctx.lineWidth = main_stroke_range;


}

function Rectangle_mousemove (event, ctx, canvas) {
    if(MouseKeepPress){

        

        ctx.putImageData(temp_canvas, 0, 0);

        destinationX = event.offsetX;
        destinationY = event.offsetY; 

        rec_width = destinationX - originX;
        rec_height = destinationY - originY;

        abs_rec_width = Math.abs(rec_width);
        abs_rec_height = Math.abs(rec_height);

        if(rec_width>=0 && rec_height>=0)
            ctx.strokeRect(originX, originY, abs_rec_width, abs_rec_height);
        else if(rec_width>=0 && rec_height<=0)
            ctx.strokeRect(originX, originY - abs_rec_height, abs_rec_width, abs_rec_height);
        else if(rec_width<=0 && rec_height>=0)
            ctx.strokeRect(destinationX, destinationY - abs_rec_height, abs_rec_width, abs_rec_height);
        else 
            ctx.strokeRect(destinationX, destinationY, abs_rec_width, abs_rec_height);



    }
    else
        return;
}

function Rectangle_mouseup (ctx, canvas) {
    MouseKeepPress = false;

}

//Circle function
function Circle_mousedown (event, ctx, canvas) {
    MouseKeepPress = true;
    originX = event.offsetX;
    originY = event.offsetY;

    temp_canvas = ctx.getImageData(0, 0, canvas.width, canvas.height);
    ctx.lineWidth = main_stroke_range;


}

function Circle_mousemove (event, ctx, canvas) {
    if(MouseKeepPress){


        ctx.putImageData(temp_canvas, 0, 0);

        destinationX = event.offsetX;
        destinationY = event.offsetY; 

        var dx = Math.abs(destinationX - originX);
        var dy = Math.abs(destinationY - originY);
        var radius = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2)) / 2;
        var centerX = (destinationX + originX) / 2;
        var centerY = (destinationY + originY) / 2;

        ctx.beginPath();
        ctx.arc(centerX, centerY, radius, 0,  2 * Math.PI, true);
        ctx.stroke();



    }
    else
        return;
}

function Circle_mouseup (ctx, canvas) {
    MouseKeepPress = false;


}

//Triangle function
function Triangle_mousedown (event, ctx, canvas) {
    MouseKeepPress = true;
    originX = event.offsetX;
    originY = event.offsetY;

    temp_canvas = ctx.getImageData(0, 0, canvas.width, canvas.height);
    ctx.lineWidth = main_stroke_range;


}

function Triangle_mousemove (event, ctx) {
    if(MouseKeepPress){

        ctx.putImageData(temp_canvas, 0, 0);

        destinationX = event.offsetX;
        destinationY = event.offsetY; 

        var centerX = (destinationX + originX) / 2;

        ctx.beginPath();
        ctx.moveTo(originX, destinationY);
        ctx.lineTo(destinationX, destinationY);
        ctx.lineTo(centerX, originY);
        ctx.closePath();
        ctx.stroke();



    }
    else
        return;
}

function Triangle_mouseup (ctx, canvas) {
    MouseKeepPress = false;


}








