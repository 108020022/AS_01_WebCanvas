# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    整個網頁分成左半部的canvas，以及右半部的工具欄，藉由滑鼠點選不同的工具至canvas至canvas畫圖。工具欄的最上方是調色盤，調色盤的左邊是彩虹七色，點選不同的顏色可以其為底色，讓右邊長方形做白色底色漸變以及黑色透明色漸變，點選調色盤的顏色可控制筆刷、圖形、文字的顏色。調色盤下方是按扭區，有筆刷(可在canvas上畫圖)、橡皮擦(可清除canvas上的圖案)、文字(可在指定位置輸入文字並貼上)、三角形(可印出三角形，按住滑鼠後拖曳可控制大小)、圓形(可印出圓形，按住滑鼠後拖曳可控制大小)、長方形(可印出長方形，按住滑鼠後拖曳可控制大小)、Undo(回復上一步)、Redo(返回下一步)、Restart(重作)。按扭區的下方有兩個selection，分別是選擇字體以及選擇字型大小。selection下方是一個slider，可控制筆刷、橡皮擦、圖形的畫筆大小。slider下方分別是下載(可下載目前canvas的畫面)以及上傳(可上傳圖片至canvas，並以圖片的長寬作為canvas的長寬)。


### Function description
    change_status(new_status):
        我設計按鈕在onclick時，叫出這個函式，並把status改成相對應的new_status(Ex.若按下橡皮擦按鈕，則會叫出change_status('Eraser')，此時的status就會變成'Eraser')，後面會根據status叫出不同函式。

    main():
        這是最主要的函式，在window.onload裡，做基本變數宣告、筆刷大小初始化、筆刷顏色的初始化等，滑鼠的各種監聽也在裡面，偵測到mousedown，則根據status，進到不同的mousedown函式，mousemove、mouseup也是，其他功能在底下說明。
    
    Brush的功能:分成Brush_mousedown、Brush_mousemove、Brush_mouseup三個函式。
        Brush_mousedown:
            當status='Brush'，滑鼠在canvas按下時進入，會讓MouseKeepPress變true，若mouseup則讓MouseKeepPress變false，用意是要按著滑鼠移動才能畫圖，裡面也記錄了按下時的滑鼠位置，以及設定筆刷顏色大小。
        Brush_mousemove:
            當status='Brush'，滑鼠在canvas滑動時進入，而且要按著滑鼠(MouseKeepPress==true)，裡面記錄了新的滑鼠位置，並畫一條從原位置往新位置的線，最後把新位置存入原位置繼續等待滑鼠移動偵測。
        Brush_mouseup:
            當status='Brush'，滑鼠在canvas放開時進入，此時將MouseKeepPress變false即可。
    
    Eraser的功能:分成Eraser_mousedown、Eraser_mousemove、Eraser_mouseup三個函式。
        Eraser_mousedown:
            當status='Eraser'，滑鼠在canvas按下時進入，會讓MouseKeepPress變true，若mouseup則讓MouseKeepPress變false，用意是要按著滑鼠移動才能擦除，記錄了按下時的滑鼠位置，並根據畫筆大小以長方形清除該位置，以及設定畫筆大小。
        Eraser_mousemove:
            當status='Eraser'，滑鼠在canvas滑動時進入，而且要按著滑鼠(MouseKeepPress==true)，裡面記錄了新的滑鼠位置，並根據畫筆大小以長方形清除該位置，為了使原位置與新位置之間區域也連貫清除，先記錄了他們之間的斜率，若斜率為正，則清除原位置左上點到新位置左上點到新位置右下點到原位置右下點的長方形區域，若斜率為負，則清除原位置左下點到新位置左下點到新位置右上點到原位置右上點的長方形區域，最後把新位置存入原位置繼續等待滑鼠移動偵測。
        Eraser_mouseup:
            當status='Eraser'，滑鼠在canvas放開時進入，此時將MouseKeepPress變false即可。

    Rectangle的功能:分成Rectangle_mousedown、Rectangle_mousemove、Rectangle_mouseup三個函式。
        Rectangle_mousedown:
            當status='Rectangle'，滑鼠在canvas按下時進入，會讓MouseKeepPress變true，若mouseup則讓MouseKeepPress變false，用意是要按著滑鼠移動才能畫長方形，裡面也記錄了按下時的滑鼠位置，以及設定畫筆大小，最後利用getImageData函式記錄了按下去時canvas的畫面。
        Rectangle_mousemove:
            當status='Rectangle'，滑鼠在canvas滑動時進入，而且要按著滑鼠(MouseKeepPress==true)，裡面記錄了新的滑鼠位置，並設定長方形的寬為新位置的X減去原位置的X取絕對值、長方形的高為新位置的Y減去原位置的Y取絕對值，最後根據長寬以及原位置畫出長方形，值得注意一點的是，若滑鼠還沒放開要利用putImageData回到原本的畫面，才不會留下長方形殘影。   
        Rectangle_mouseup:
            當status='Rectangle'，滑鼠在canvas放開時進入，此時將MouseKeepPress變false即可。

    Circle的功能:分成Circle_mousedown、Circle_mousemove、Circle_mouseup三個函式。
        Circle_mousedown:
            當status='Circle'，滑鼠在canvas按下時進入，會讓MouseKeepPress變true，若mouseup則讓MouseKeepPress變false，用意是要按著滑鼠移動才能畫圓形，裡面也記錄了按下時的滑鼠位置，以及設定畫筆大小，最後利用getImageData函式記錄了按下去時canvas的畫面。
        Circle_mousemove:
            當status='Circle'，滑鼠在canvas滑動時進入，而且要按著滑鼠(MouseKeepPress==true)，裡面記錄了新的滑鼠位置，並設定圓形的原點為新位置與原位置的中點、圓形的半徑為新位置與原位置的距離除以二，最後根據半徑以及原點畫出圓形，值得注意一點的是，若滑鼠還沒放開要利用putImageData回到原本的畫面，才不會留下圓形殘影。
        Circle_mouseup:
            當status='Circle'，滑鼠在canvas放開時進入，此時將MouseKeepPress變false即可。

    Triangle的功能:分成Triangle_mousedown、Triangle_mousemove、Triangle_mouseup三個函式。
        Triangle_mousedown:
            當status='Triangle'，滑鼠在canvas按下時進入，會讓MouseKeepPress變true，若mouseup則讓MouseKeepPress變false，用意是要按著滑鼠移動才能畫三角形，裡面也記錄了按下時的滑鼠位置，以及設定畫筆大小，最後利用getImageData函式記錄了按下去時canvas的畫面。
        Triangle_mousemove:
            當status='Triangle'，滑鼠在canvas滑動時進入，而且要按著滑鼠(MouseKeepPress==true)，裡面記錄了新的滑鼠位置，宣告一個centerX是新位置與原位置X方向上的中點，並從新位置的X新位置的Y劃線到原位置的X新位置的Y，在劃線到centerX原位置的Y，即完成三角形，值得注意一點的是，若滑鼠還沒放開要利用putImageData回到原本的畫面，才不會留下三角形殘影。  
        Triangle_mouseup:
            當status='Triangle'，滑鼠在canvas放開時進入，此時將MouseKeepPress變false即可。

    Redo及Undo功能:
        分別有宣告CanvasHistory_undo及CanvasHistory_redo兩個陣列來存放每一次操作的畫面。在main()函式裡，最一開始會利用getImageData將空白的的canvas(origin_canvas)push至CanvasHistory_undo裡，當每一次滑鼠在canvas放開，就會紀錄下當時的畫面push進去CanvasHistory_undo。若偵測到Undo的按鈕按下，就會利用stack的原理把CanvasHistory_undo最上層的pop掉並存給CanvasHistory_redo陣列，並利用putImageData印出此時CanvasHistory_undo最上層的影像。Redo同理，若偵測到按鈕按下，就把CanvasHistory_redo最上層的pop掉並存給CanvasHistory_undo陣列，並利用putImageData印出此時CanvasHistory_redo最上層的影像。

    Restart:
        在main()裡，若偵測到Restart的按鈕按下，則會利用putImageData印出當初存的origin_canvas影像，並把CanvasHistory_undo及CanvasHistory_redo兩陣列清空。
    
    調色盤:
        ColorRect(color_ctx)及ColorBox(color_ctx, color)函式是為了顯示出調色盤顏色，ColorRect利用createLinearGradient函式製造出彩虹七色漸層，ColorBox會傳入ColorRect的底色，並利用createLinearGradient函式製造出長是底色白色漸層以及寬是黑色透明色漸層。

        選取顏色的方法是，調色盤我是設計成另一個canvas(color_canvas)，在main()裡，若color_canvas偵測到mousedown，會記錄下當前的滑鼠位置，並利用RGB_of_that_point函式得知該位置的RGBA值，最後將main_color換成該顏色即可。
    
    Text_mousedown函式:
        前置作業將html新增一個元素<textarea>(TextBox)，並把z-index設成1，canvas的z-index設成5，如此會把textarea隱藏起來。
        分成兩個status，一開始按下滑鼠時，會進入input_the_text的status，會記錄下滑鼠當前位置並存給TextBox的left及top，並把z-index設成6顯示出來。
        當下一次按下滑鼠時，進到另一個status，把z-index設成1讓TextBox隱藏，並利用Draw_The_Text函式將文字畫在canvas上。
    
    字體及字型大小:
        在main()，當字體或字型大小改變時，main_font_family及main_font_size就會分別存取該選項的value，並在Draw_The_Text函式將字體及大小設定好。

    改變畫筆大小:
        在main()，當slider值改變時，main_stroke_range就會存取slider的value如此改變畫筆、橡皮猜、圖形的畫筆大小。

    Download:
        在main()，當download的按鈕onclick時，會把其href改成canvas.toDataURL()，並"MyCanvas.png"成為他的檔名。

    Upload:
        在main()，當upload的按鈕onclick時，會宣告一個Img，並利用createObjectURL存取使用者選取的照片，同時設定canvas長寬是此照片長寬。
    
    Mouse Icon:
        根據不同的status，修改canvas.style['cursor']。
    



### Gitlab page link

    https://108020022.gitlab.io/AS_01_WebCanvas


### Others (Optional)

    

<style>
#CanvasRegion_Adjustion{
    margin: 0 10;
    width: 1100px;
    height: 600px;
    float: left;
}

#Tool_Menu{
    float: left;
    border:4px solid PapayaWhip;
    margin: 0 12;
    width: 350px;
    height: 700px;
    position: relative;
}

#color_choose_region{
    width: 300;
    height: 170;
    
}

#font_adjust_region {
    position: absolute;
    left: 10;
    top: 500;
    height: 40;
    width: 300;
}

#slider_region {
    position: absolute;
    left: 10;
    top: 560;
    height: 20;
    width: 300;
}



#upload_download_region {
    position: absolute;
    left: 10;
    top: 600;
    height: 80;
    width: 300;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content:space-around;
}


.row{
    width: 100%;
    text-align: center;
    line-height: 80px;
}
.btn {
    width: 170px;
    height: 60px;
    background: OldLace;
    font-size: 22px;
    transition: background 0.3s;
    box-shadow: none;
    border: 0;			
}


.btn:hover {
    background: white;
}	

#myCanvas {
    z-index: 5;
    position: absolute;
    float: left;
    top: 10;
    left: 15;
    border-top:3px solid 	BurlyWood;
    border-left:3px solid 	BurlyWood;
    border-right:5px solid Tan;
    border-bottom:5px solid Tan;
    background-color: white;
    width: 1100px;
    height: 600px;
    
}

#color_choose {
    z-index: 5;
    position: absolute;
    border: transparent;
    height: 170;
    width: 300;
}

#TextBox {
    font: 28px DFKai-sb;
    background-color: transparent;
    resize: none;
    z-index: 1;
    border: transparent;
    word-break: break-all;
    float: left;
    position: absolute;
    top: 10;
    left: 10;
}
</style>